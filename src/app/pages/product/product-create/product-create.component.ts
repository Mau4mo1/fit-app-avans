import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { category } from 'src/domain/category';
import { supermarket } from 'src/domain/supermarket';
import { SupermarketService } from '../../supermarket/supermarket.service';
import { ProductService } from '../product.service';

@Component({
	selector: 'app-product-create',
	templateUrl: './product-create.component.html',
	styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
	addProductForm = new FormGroup({
		name: new FormControl('', Validators.required),
		image: new FormControl('', Validators.required),
		calories: new FormControl('', Validators.required),
		carbohydrates: new FormControl('', Validators.required),
		protein: new FormControl('', Validators.required),
		fats: new FormControl('', Validators.required),
		fibers: new FormControl('', Validators.required),
	});

	supermarketId: string | null = null;
	categoryId: string | null = null;
	currentCategory: category | null = null;
	constructor(private route: ActivatedRoute, private productService: ProductService, private router: Router) {

	}

	ngOnInit(): void {
		this.supermarketId = this.route.snapshot.paramMap.get('id');
		this.categoryId = this.route.snapshot.paramMap.get('categoryId');
	}
	async onSubmit(): Promise<void> {
		this.productService.createProduct(this.addProductForm.value, this.categoryId!).subscribe(
			this.router.navigate(['/supermarket/' + this.route.snapshot.paramMap.get('id')])
		)
	}
}
