import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { category } from "src/domain/category";
import { map, tap } from 'rxjs/operators';
import { product } from "src/domain/product";

@Injectable({
	providedIn: 'root'
})
export class ProductService {


	baseUrl: string = "https://fit-api-avans.herokuapp.com/categories/";

	constructor(
		public readonly http: HttpClient
	) { }

	getProductById(categoryId: string, productId: string): Observable<product> {
		return this.http.get<product>(this.baseUrl + categoryId + '/products/' + productId)
			.pipe();
	}

	// id here is category id
	createProduct(param: product, categoryId: string): any {
		//let product:product = new product()

		return this.http.post<any>(this.baseUrl + categoryId + '/products', param).pipe();
	}

	updateProduct(param: product, id: string, categoryId: string): Observable<product> {
		return this.http.put<any>(this.baseUrl + categoryId + '/products/' + id, param).pipe();
	}

	// id here is product id
	deleteProduct(id: string, categoryId: string): any {
		return this.http.delete<any>(this.baseUrl + categoryId + '/products/' + id).pipe();
	}
}
