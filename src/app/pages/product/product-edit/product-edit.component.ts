import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { category } from 'src/domain/category';
import { product } from 'src/domain/product';
import { supermarket } from 'src/domain/supermarket';
import { SupermarketService } from '../../supermarket/supermarket.service';
import { ProductService } from '../product.service';

@Component({
	selector: 'app-product-edit',
	templateUrl: './product-edit.component.html',
	styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

	editProductForm = new FormGroup({
		name: new FormControl('', Validators.required),
		image: new FormControl('', Validators.required),
		calories: new FormControl('', Validators.required),
		carbohydrates: new FormControl('', Validators.required),
		protein: new FormControl('', Validators.required),
		fats: new FormControl('', Validators.required),
		fibers: new FormControl('', Validators.required),
	});

	categoryId: string | null = null;
	currentProduct: product | null = null;

	constructor(private route: ActivatedRoute, private productService: ProductService, private router: Router) {

	}

	ngOnInit(): void {
		this.categoryId = this.route.snapshot.paramMap.get('categoryId');

		this.productService.getProductById(this.route.snapshot.paramMap.get('categoryId')!, this.route.snapshot.paramMap.get('productId')!).subscribe(
			(result: product) => {
				this.currentProduct = result;
				this.editProductForm.patchValue(this.currentProduct)
			}
		)
	}
	async onSubmit(): Promise<void> {
		if (this.currentProduct != undefined) {

			this.currentProduct.name = this.editProductForm.value.name;
			this.currentProduct.image = this.editProductForm.value.image;
			this.currentProduct.carbohydrates = this.editProductForm.value.carbohydrates;
			this.currentProduct.fats = this.editProductForm.value.fats;
			this.currentProduct.fibers = this.editProductForm.value.fibers;
			this.currentProduct.protein = this.editProductForm.value.protein;
			this.currentProduct.calories = this.editProductForm.value.calories;


			this.productService.updateProduct(this.currentProduct, this.currentProduct._id, this.route.snapshot.paramMap.get('categoryId')!).subscribe(
				(result:any)=>{
					this.router.navigate(['/supermarket/' + this.route.snapshot.paramMap.get('id')! + '/categories/' + this.route.snapshot.paramMap.get('categoryId')! + '/' + this.currentProduct?._id]);
				}
			);
		}
	}
}
