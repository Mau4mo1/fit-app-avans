import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { category } from 'src/domain/category';
import { product } from 'src/domain/product';
import { supermarket } from 'src/domain/supermarket';
import { user } from 'src/domain/user';
import { CartComponent } from '../../cart/cart.component';
import { CartService } from '../../cart/cart.service';
import { SupermarketService } from '../../supermarket/supermarket.service';
import { UserEditService } from '../../user/user-edit.service';
import { ProductService } from '../product.service';

@Component({
	selector: 'app-product-details',
	templateUrl: './product-details.component.html',
	styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

	supermarketId: string | null = null;
	productId: string | null = null;
	categoryId: string | undefined;
	product: product | undefined;

	constructor(private route: ActivatedRoute, private productService: ProductService, public userService: UserEditService, public cartService: CartService, private router: Router, private snackBar: MatSnackBar) {

	}

	ngOnInit(): void {
		this.productService
			.getProductById(
				this.route.snapshot.paramMap.get('categoryId')!,
				this.route.snapshot.paramMap.get('productId')!)
			.subscribe(
				(result: product) => {
					this.product = result;
				}
			)
	}

	addToMenu(): void {
		this.cartService.addToCart(this.product!).subscribe(
			(result: any) => {
				this.snackBar.open("Het product is toegevoegd!")
			}
		);
	}

}
