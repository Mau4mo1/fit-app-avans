import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { supermarket } from 'src/domain/supermarket';
import { SupermarketService } from '../supermarket.service';

@Component({
	selector: 'app-supermarket-edit',
	templateUrl: './supermarket-edit.component.html',
	styleUrls: ['./supermarket-edit.component.css']
})
export class SupermarketEditComponent implements OnInit {

	editSupermarketForm = new FormGroup({
		name: new FormControl('',Validators.required),
		image: new FormControl('',Validators.required)
	});

	supermarketId: string|null = null;

	currentSupermarket:supermarket | undefined;

	constructor(private route: ActivatedRoute, private supermarketService: SupermarketService,private router: Router) {

	}

	ngOnInit(): void {
		this.supermarketId = this.route.snapshot.paramMap.get('id');
		this.supermarketService.getSupermarketById(this.supermarketId!).subscribe((result: supermarket) => {
			this.currentSupermarket = result
			this.editSupermarketForm.patchValue(result);
		});
	}
	onSubmit(): void {
		this.supermarketService.updateSupermarket(this.editSupermarketForm.value, this.currentSupermarket!._id).subscribe( {next: ()=>{
			this.currentSupermarket = this.editSupermarketForm.value;
			this.router.navigate(['/supermarket' ]);
			this.router.navigate(['/supermarket/' + this.supermarketId]);
		} });

	}

}
