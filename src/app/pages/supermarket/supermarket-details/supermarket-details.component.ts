import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { supermarket } from 'src/domain/supermarket';
import { user } from 'src/domain/user';
import { UserEditService } from '../../user/user-edit.service';
import { SupermarketService } from '../supermarket.service';

@Component({
  selector: 'app-supermarket-details',
  templateUrl: './supermarket-details.component.html',
  styleUrls: ['./supermarket-details.component.css']
})
export class SupermarketDetailsComponent implements OnInit {

	supermarketId: string|null = null;

	currentSupermarket:supermarket | undefined;

	constructor(private route: ActivatedRoute, private supermarketService:SupermarketService, public userService : UserEditService) {

	}

	ngOnInit(): void {
		this.supermarketId = this.route.snapshot.paramMap.get('id');
		this.supermarketService.getSupermarketById(this.supermarketId!).subscribe((result: supermarket) => this.currentSupermarket = result);
	}
	delete(categoryId: string) : void{
		this.currentSupermarket?.categories.splice(this.currentSupermarket?.categories.findIndex(category => category._id == categoryId), 1);
		this.supermarketService.updateSupermarket(this.currentSupermarket!, this.supermarketId!).subscribe();
	}
}
