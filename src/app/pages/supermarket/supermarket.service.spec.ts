import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SupermarketService } from './supermarket.service';

describe('SupermarketService', () => {
  let service: SupermarketService;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    service = TestBed.inject(SupermarketService);
    http = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('HttpClientTesting is expected', () => {
    expect(http).toBeTruthy
  });
});
