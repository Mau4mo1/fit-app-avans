import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { supermarket } from 'src/domain/supermarket';
import { user } from 'src/domain/user';
import { AuthenticationService } from '../../home/authentication.service';
import { UserEditService } from '../../user/user-edit.service';
import { SupermarketService } from '../supermarket.service';

@Component({
	selector: 'app-supermarket-create',
	templateUrl: './supermarket-create.component.html',
	styleUrls: ['./supermarket-create.component.css']
})
export class SupermarketCreateComponent implements OnInit {
	addSupermarketForm = new FormGroup({
		name: new FormControl('', Validators.required),
		image: new FormControl('',Validators.required)
	});

	supermarketId: string | null = null;
	currentSupermarket:supermarket | undefined;
	currentUser: user | undefined;

	constructor(private route: ActivatedRoute, private supermarketService: SupermarketService, private router:Router, private userService: UserEditService) {

	}
	ngOnInit(): void {
		this.supermarketId = this.route.snapshot.paramMap.get('id');

	}

	onSubmit(): void {
		this.supermarketService.createSupermarket(this.addSupermarketForm.value).subscribe(
			(result:supermarket)=>{
				setTimeout(() => {
					this.router.navigate(['/supermarket'])
				}, 50);
			}
		);
	}
}
