import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { supermarket } from 'src/domain/supermarket';


@Injectable({
	providedIn: 'root'
})
export class SupermarketService {

	supermarkets: supermarket[] = [];
	baseUrl: string = "https://fit-api-avans.herokuapp.com/supermarkets";
	constructor(
		public readonly http: HttpClient
	) {
	}

	getSupermarkets(): Observable<supermarket[]> {
		return this.http.get<supermarket[]>(this.baseUrl)
			.pipe();
		//map((response) => response.body.result));
	}
	updateSupermarket(param: supermarket, id: string): Observable<supermarket> {
		return this.http.put<any>(this.baseUrl + '/' + id, param).pipe();
	}
	getSupermarketById(id: string): Observable<supermarket> {

		return this.http.get<supermarket>(this.baseUrl + '/' + id).pipe();
	}
	createSupermarket(param: supermarket): Observable<supermarket> {
		this.supermarkets.push(param);

		return this.http.post<any>(this.baseUrl, param).pipe();
	}
	deleteSupermarket(id: string): any {
		return this.http.delete<any>(this.baseUrl + '/' + id).pipe();
	}
}
