import { Component, OnInit } from '@angular/core';
import { supermarket } from 'src/domain/supermarket';
import { user } from 'src/domain/user';
import { UserEditService } from '../../user/user-edit.service';
import { SupermarketService } from '../supermarket.service';

@Component({
	selector: 'app-supermarket-list',
	templateUrl: './supermarket-list.component.html',
	styleUrls: ['./supermarket-list.component.css']
})
export class SupermarketListComponent implements OnInit {

	public supermarkets: supermarket[] = []
	public currentUser: user | undefined;
	constructor(private supermarketService: SupermarketService, public userService: UserEditService) {

	}

	ngOnInit(): void {
		this.supermarketService.getSupermarkets().subscribe((results: supermarket[]) => this.supermarkets = results)
	}
	delete(id: string): void{
		this.supermarketService.deleteSupermarket(id).subscribe(
			(result:supermarket)=>{
				this.supermarkets = this.supermarkets.filter(supermarket => supermarket._id !== id)
			}
		);
	}

}
