import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { product } from 'src/domain/product';
import { user } from 'src/domain/user';
import { UserEditService } from '../user/user-edit.service';
import { CartService } from './cart.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

	currentCart: product[] | undefined;

	user: user | undefined

	totalProduct: product;
	totalProductAMeal: product;
	constructor(private router: Router, private cartService: CartService, private snackBar: MatSnackBar, private userService: UserEditService) {
		this.totalProduct = new product('', '', '', 0, 0, 0, 0, 0)
		this.totalProductAMeal = new product('', '', '', 0, 0, 0, 0, 0)
	}

	ngOnInit(): void {
		this.cartService.getCart().subscribe(
			(result: any) => {
				this.currentCart = result.products;
				this.calculateTotal();
			}
		)
		this.userService.getUserById(localStorage.getItem('userId')!).subscribe(
			(result: user) => {
				this.user = result;
			}
		)

	}

	calculateTotal() {
		this.totalProduct = new product('', '', '', 0, 0, 0, 0, 0)

		this.currentCart?.forEach((product: product) => {
			this.totalProduct.calories = +this.totalProduct.calories + +product.calories;
			this.totalProduct.fats = +this.totalProduct.fats + +product.fats;
			this.totalProduct.protein = +this.totalProduct.protein + +product.protein;
			this.totalProduct.fibers = +this.totalProduct.fibers + +product.fibers;
			this.totalProduct.carbohydrates = +this.totalProduct.carbohydrates + +product.carbohydrates;
			if (this.user?.preference?.mealsADay != undefined) {
				this.totalProductAMeal.calories = (+this.totalProduct.calories + +product.calories) / +this.user?.preference?.mealsADay;
				this.totalProductAMeal.fats = (+this.totalProduct.fats + +product.fats) / +this.user?.preference?.mealsADay;
				this.totalProductAMeal.protein = (+this.totalProduct.protein + +product.protein) / +this.user?.preference?.mealsADay;
				this.totalProductAMeal.fibers = (+this.totalProduct.fibers + +product.fibers) / +this.user?.preference?.mealsADay;
				this.totalProductAMeal.carbohydrates = (+this.totalProduct.carbohydrates + +product.carbohydrates) / +this.user?.preference?.mealsADay;
			}

		})
	}

	delete(id: number) {
		this.cartService.deleteProduct(id.toString()).subscribe(
			(result: any) => {
				if (result) {
					this.snackBar.open("Het product is verwijderd! :)")

					this.currentCart?.splice(id, 1);

					this.calculateTotal()
				} else {
					this.snackBar.open("Het verwijderen is niet gelukt! :(");
				}
			}
		);
	}
}
