import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { product } from 'src/domain/product';
import { AuthenticationService } from '../home/authentication.service';
import { UserEditService } from '../user/user-edit.service';

@Injectable({
	providedIn: 'root'
})
export class CartService {

	constructor(private http: HttpClient, private router: Router, private userService: UserEditService) { }

	baseurl: string = 'https://fit-api-avans.herokuapp.com/users/';

	addToCart(product: product) : Observable<any>{
		return this.http.post<any>(this.baseurl + localStorage.getItem('userId') + '/carts',product)
	}

	getCart() : Observable<product[]>{
		return this.http.get<any>(this.baseurl + localStorage.getItem('userId') + '/carts')
	}
	deleteProduct(productId:string): any{
		return this.http.delete<any>(this.baseurl + localStorage.getItem('userId') + '/carts/' + productId)
	}
}
