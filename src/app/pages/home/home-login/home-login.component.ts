import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
	selector: 'app-home-login',
	templateUrl: './home-login.component.html',
	styleUrls: ['./home-login.component.css']
})
export class HomeLoginComponent implements OnInit {

	loginForm = new FormGroup({
		email: new FormControl('',[Validators.email,Validators.required]),
		password: new FormControl('',[Validators.required])
	});

	constructor(
		private authenticationService: AuthenticationService,
		private router: Router) { }
	ngOnInit(): void {
		if (this.authenticationService.isLoggedIn()) {
			this.router.navigate(['/']);
		}
	}

	onSubmit(): void {
		this.authenticationService.login(this.loginForm.value);
	}
}
