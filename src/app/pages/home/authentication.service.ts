import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { user } from 'src/domain/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationService {

	baseurl: string = 'https://fit-api-avans.herokuapp.com/users/';

	constructor(private http: HttpClient, private router: Router, private snackBar: MatSnackBar) { }

	login(params: any) {
		this.http
			.post<any>(`${this.baseurl}login`, params)
			.pipe(catchError(val => of(val)))
			.subscribe((response: any) => {
				if (response.token) {
					this.setUserSession(response);
					this.router.navigate(['/']);
				} else {
					this.snackBar.open("Een van de velden is niet juist ingevuld of de gebruiker bestaat niet!");
				}
			});
	}

	register(user: user): any {
		return this.http
			.post<any>(`${this.baseurl}register`, user)
			.pipe(catchError(val => of(val)))
			.subscribe((response: any) => {
				if (response.token) {
					this.setUserSession(response);
					this.router.navigate(['/']);
				} else {
					this.snackBar.open("Een van de velden is niet juist ingevuld of de gebruiker bestaat al!");
				}
			});
	}

	private setUserSession(response: any) {
		localStorage.setItem('id_token', response.token);
		localStorage.setItem('expires_at', response.expires);
		localStorage.setItem('userId', response.user._id);
	}
	logout() {
		localStorage.removeItem('id_token');
		localStorage.removeItem('expires_at');
		localStorage.removeItem('userId');
	}
	public isLoggedIn() {
		if (new Date() > this.getSessionExperitation()!) {
			return false;
		} else {
			return localStorage.getItem('id_token') != undefined;
		}
	}
	getSessionExperitation() {
		const dateString: string | null = localStorage.getItem('expires_at');
		if (dateString) {
			return new Date(dateString);
		} else {
			return null;
		}
	}
}
