import { Component, OnInit } from '@angular/core'
import { UseCase } from './usecases/usecase.model'

@Component({
	selector: 'app-about',
	templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {
	readonly PLAIN_USER = 'Reguliere gebruiker'
	readonly ADMIN_USER = 'Administrator'

	useCases: UseCase[] = [
		{
			id: 'UC-01',
			name: 'Inloggen',
			description: 'Hiermee logt een bestaande gebruiker in.',
			scenario: [
				'Gebruiker vult email en password in en klikt op Login knop.',
				'De applicatie valideert de ingevoerde gegevens.',
				'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
			],
			actor: this.PLAIN_USER,
			precondition: 'Heeft een account',
			postcondition: 'De actor is ingelogd'
		},
		{
			id: 'UC-02',
			name: 'Registreren',
			description: 'Hiermee registreerd een gebruiker een account',
			scenario: ['Gebruiker vult benodigde waarden in', 'De applicatie voert de nodige validatie uit', 'Indien gegevens correct zijn/haar redirect de applicatie naar het startscherm waar de gebruiker een overzicht te zien krijgt van zijn/haar account.'],
			actor: this.PLAIN_USER,
			precondition: 'Geen',
			postcondition: 'Gebruiker heeft een account'
		},
		{
			id: 'UC-3',
			name: 'Supermarkt, Categorie en Product Create',
			description: 'Een administrator moet deze 3 entiteiten kunnen aanmaken',
			scenario: [' Administrator vult de benodige waarden in', 'Een aantal validatie stappen worden uitgevoerd', 'Entiteit wordt toegevoegd', 'Administrator wordt naar index pagina van entiteit gestuurd.'],
			actor: this.ADMIN_USER,
			precondition: 'Bij categorie moet een supermarkt aanwezig zijn en bij product een categorie',
			postcondition: 'Een entiteit is aangemaakt'
		},
		{
			id: 'UC-04',
			name: 'Supermarkt, Categorie, en Product Delete',
			description: 'Een administrator moet zijn EIGEN gemaakte entiteiten kunnen verwijderen',
			scenario: ['Administrator drukt op delete knop', 'API kijkt of dit wel de eigen entiteit is', 'Entiteit is verwijderd, adminnistrator blijft op index pagina'],
			actor: this.ADMIN_USER,
			precondition: 'Een entiteit bestaat',
			postcondition: 'Een entiteit bestaat niet meer'
		},
		{
			id: 'UC-05',
			name: 'Supermarkt, Categorie, en Product update',
			description: 'Een adminstrator moet zijn EIGEN gemaakte entiteiten kunnen bewerken',
			scenario: ['Administrator drukt op edit knop', 'Administrator wordt naar update pagina geredirect', 'Administrator vult benodigde gegevens in', 'Administrator drukt op bewerk knop', 'Administrator wordt teruggestuurd naar index pagina'],
			actor: this.ADMIN_USER,
			precondition: 'Een entiteit bestaat',
			postcondition: 'Een entiteit is bijgewerkt'
		},
		{
			id: 'UC-06',
			name: 'Een gebruiker kan een vriend toegoeven',
			description: 'Een gebruiker moet een vriend kunnen toevoegen',
			scenario: ['Gebruiker drukt op voeg toe als vriend knop', 'Gebruiker is nu vriend met ...'],
			actor: this.PLAIN_USER,
			precondition: 'Gebruiker is ingelogd',
			postcondition: 'Gebruiker heeft nu FRIENDS relatie met andere gebruiker'
		},
		{
			id: 'UC-07',
			name: 'Een gebruiker kan een product liken',
			description: 'Een gebruiker moet een product kunnen liken',
			scenario: ['Gebruiker drukt op like product knop', 'Gebruiker LIKED nu product'],
			actor: this.PLAIN_USER,
			precondition: 'Een product bestaat en gebruiker is ingelogd',
			postcondition: 'Gebruiker heeft nu LIKED relatie met product'
		},
		{
			id: 'UC-08',
			name: 'Aanbevolen producten bekijken',
			description: 'Een gebruiker moet aanbevolen producten krijgen op basis van de producten die vrienden hebben geliked',
			scenario: ['Gebruiker bevind zich op reccomended pagina', 'Applicatie toond een pagina met producten die vrienden hebben geliked'],
			actor: this.PLAIN_USER,
			precondition: 'Gebruiker heeft producten geliked en vrienden toegevoegd',
			postcondition: ''
		},
	]

	constructor() { }

	ngOnInit() { }
}
