import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { category } from "src/domain/category";
import { map, tap } from 'rxjs/operators';
import { ActivatedRoute } from "@angular/router";

@Injectable({
	providedIn: 'root'
})
export class CategoryService {

	baseUrl = 'https://fit-api-avans.herokuapp.com/categories/'

	constructor(
		private http: HttpClient,

	) {

	}

	getCategoryById(categoryId: string): Observable<category> {
		return this.http.get<category>(this.baseUrl + categoryId)
			.pipe();
	}

	createCategory(params: any, id:string): any {
		return this.http.post<void>(this.baseUrl  + id, params).pipe();
	}

	updateCategory(params: category, id: string): any {
		return this.http.put<void>(this.baseUrl + id, params).pipe();
	}

	deleteCategory(): void {

	}
}
