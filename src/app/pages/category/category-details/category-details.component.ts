import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { category } from 'src/domain/category';
import { product } from 'src/domain/product';
import { supermarket } from 'src/domain/supermarket';
import { user } from 'src/domain/user';
import { ProductService } from '../../product/product.service';
import { SupermarketService } from '../../supermarket/supermarket.service';
import { UserEditService } from '../../user/user-edit.service';
import { CategoryService } from '../category.service';

@Component({
	selector: 'app-category-details',
	templateUrl: './category-details.component.html',
	styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent implements OnInit {

	currentCategory: category | undefined;

	constructor(private route: ActivatedRoute, private categoryService: CategoryService, public userService: UserEditService, private productService: ProductService) {

	}

	ngOnInit(): void {
		this.categoryService.getCategoryById(this.route.snapshot.paramMap.get('categoryId')!).subscribe(
			(result: category) => {
				this.currentCategory = result
			}
		);
	}

	delete(productId: string): void {
		this.productService.deleteProduct(productId, this.route.snapshot.paramMap.get('categoryId')!).subscribe(
			this.currentCategory?.products.splice(
				this.currentCategory?.products.findIndex(product => product._id == productId), 1
			)
		)
	}
}
