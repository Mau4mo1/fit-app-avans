import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { supermarket } from 'src/domain/supermarket';
import { SupermarketService } from '../../supermarket/supermarket.service';
import { CategoryService } from '../category.service';

@Component({
	selector: 'app-category-create',
	templateUrl: './category-create.component.html',
	styleUrls: ['./category-create.component.css']
})
export class CategoryCreateComponent implements OnInit {
	addCategoryForm = new FormGroup({
		name: new FormControl('',Validators.required),
		image: new FormControl('',Validators.required)
	});

	supermarketId: string | null = null;
	currentSupermarket: supermarket | null = null;
	constructor(private route: ActivatedRoute, private categoryService: CategoryService, private router: Router) {

	}

	ngOnInit(): void {
		this.supermarketId = this.route.snapshot.paramMap.get('id');
	}
	async onSubmit(): Promise<void> {
		this.categoryService.createCategory(this.addCategoryForm.value, this.supermarketId!).subscribe(
			this.router.navigate(['/supermarket/' + this.route.snapshot.paramMap.get('id')!])
		)
	}

}
