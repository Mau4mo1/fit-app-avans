import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { category } from 'src/domain/category';
import { product } from 'src/domain/product';
import { supermarket } from 'src/domain/supermarket';
import { SupermarketService } from '../../supermarket/supermarket.service';
import { CategoryService } from '../category.service';

@Component({
	selector: 'app-category-edit',
	templateUrl: './category-edit.component.html',
	styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {
	editCategoryForm = new FormGroup({
		name: new FormControl('', Validators.required),
		image: new FormControl('', Validators.required)
	});

	currentCategory: category | undefined;

	constructor(private route: ActivatedRoute, private categoryService: CategoryService, private router: Router) {
	}

	ngOnInit(): void {
		this.categoryService.getCategoryById(this.route.snapshot.paramMap.get('categoryId')!).subscribe(
			(result: category) => {
				this.currentCategory = result;
				this.editCategoryForm.patchValue(this.currentCategory)
			}
		)
	}
	async onSubmit(): Promise<void> {
		if (this.currentCategory != undefined) {

			this.currentCategory.name = this.editCategoryForm.value.name;
			this.currentCategory.image = this.editCategoryForm.value.image;

			this.categoryService.updateCategory(this.currentCategory, this.route.snapshot.paramMap.get('categoryId')!).subscribe(
				(result:any)=>{
					this.router.navigate(['/supermarket/' + this.route.snapshot.paramMap.get('id')! + '/categories/' + this.route.snapshot.paramMap.get('categoryId')!]);
				}
			);
		}
	}
}
