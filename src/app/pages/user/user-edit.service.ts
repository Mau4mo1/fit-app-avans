import { HttpClient } from '@angular/common/http';
import { decimalDigest } from '@angular/compiler/src/i18n/digest';
import { Injectable } from '@angular/core';
import { user } from 'src/domain/user';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
	providedIn: 'root'
})
export class UserEditService {

	baseUrl: string = "https://fit-api-avans.herokuapp.com/users";
	currentUser: user | undefined;

	constructor(
		public readonly http: HttpClient
	) {
		this.getUserById(localStorage.getItem('userId')!).pipe().subscribe(
			(result: user) => {
				this.currentUser = result
			}
		)
	}

	getUserById(id: string): Observable<user> {
		return this.http.get<user>(this.baseUrl + '/' + id)
	}

	getUsers(): Observable<user[]> {
		return this.http.get<user[]>(this.baseUrl)
		//map((response) => response.body.result));
	}

	createUser(param: user): Observable<user> {
		return this.http.post<any>(this.baseUrl, param);
	}
	deleteUser(id: string): Observable<user> {
		return this.http.delete<any>(this.baseUrl + '/' + id);
	}
	updateUser(param: user, id: string): Observable<user> {
		return this.http.put<any>(this.baseUrl + '/' + id, param);
	}
	addFriend(friendId: string, loggedUserId: string): Observable<void> {
		return this.http.post<any>(this.baseUrl + '/friends', { "currentUser": loggedUserId, "friendUser": friendId });
	}
	getFriends(): Observable<user[]> {
		return this.http.get<user[]>(this.baseUrl + '/friends/' + this.currentUser?._id);
	}
}
