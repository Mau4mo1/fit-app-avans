import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UserEditService } from './user-edit.service';
import { user } from 'src/domain/user';
import { of } from 'rxjs';
import { preference } from 'src/domain/preference';

const expectedUserData: user[] = [
	{
		_id: 'mongo_id',
		name: 'maurice',
		email: 'Maurice@outlook.com',
		dateOfBirth: new Date(2001, 8, 24),
		isActive: true,
		role: 1,
		password: 'Admin123',
		preference: new preference(
			100,
			50,
			30,
			50,
			40,
			3
		),
		cart: []
	},
	{
		_id: 'mongo_id',
		name: 'Luuk',
		password: 'luukAdmin123',
		email: 'luuk@outlook.com',
		dateOfBirth: new Date(2001, 8, 24),
		isActive: true,
		role: 1, preference: new preference(
			100,
			50,
			30,
			50,
			40,
			3
		),
		cart: []
	}
];
const updatedUserData: user = {
	name: 'newUser',
	_id: 'newId',
	email: 'newemail@outlook.com',
	password: 'luukAdmin123',
	dateOfBirth: new Date(2001, 8, 24),
	isActive: true,
	role: 1,
	preference: new preference(
		100,
		50,
		30,
		50,
		40,
		3
	),
	cart: []
}
describe('UserEditService', () => {
	let service: UserEditService;
	let httpSpy: jasmine.SpyObj<HttpClient>;

	beforeEach(() => {
		httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete', 'put']);
		TestBed.configureTestingModule({
			providers: [{ provide: HttpClient, useValue: httpSpy }],
		});
		service = TestBed.inject(UserEditService);

		httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	fit('should return a list of users', (done: DoneFn) => {
		httpSpy.get.and.returnValue(of(expectedUserData));

		service.getUsers().subscribe((users: user[]) => {
			expect(users.length).toBe(2);
			expect(users[0]._id).toEqual(expectedUserData[0]._id);
			done();
		});
	});

	fit('should return a user', (done: DoneFn) => {
		httpSpy.get.and.returnValue(of(expectedUserData[0]));

		service.getUserById("1").subscribe((users: user) => {
			expect(users._id).toEqual(expectedUserData[0]._id);
			done();
		});
	});

	fit('should delete a user', (done: DoneFn) => {
		httpSpy.delete.and.returnValue(of(expectedUserData[0]));

		service.deleteUser(expectedUserData[0]._id).subscribe((users: user) => {
			expect(users._id).toEqual(expectedUserData[0]._id);
			done();
		});
	});

	fit('should create a user', (done: DoneFn) => {
		httpSpy.post.and.returnValue(of(expectedUserData[0]));

		service.createUser(expectedUserData[0]).subscribe((users: user) => {
			expect(users._id).toEqual(expectedUserData[0]._id);
			done();
		});
	});

	fit('should update a user', (done: DoneFn) => {
		httpSpy.put.and.returnValue(of(updatedUserData));

		service.updateUser(expectedUserData[0], expectedUserData[0]._id).subscribe((users: user) => {
			expect(users._id).toEqual(updatedUserData._id);
			done();
		});
	});
});
