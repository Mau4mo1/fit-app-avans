import { formatDate } from '@angular/common';
import { ngModuleJitUrl } from '@angular/compiler';
import { Component, NgModule, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDetailsComponent } from '../user-details/user-details.component';
import { user } from 'src/domain/user';
import { UserEditService } from '../user-edit.service';


@Component({
	selector: 'app-user-edit',
	templateUrl: './user-edit.component.html',
	styleUrls: ['./user-edit.component.css'],

})

export class UserEditComponent implements OnInit {
	editUserForm = new FormGroup({
		name: new FormControl('', [Validators.required, Validators.minLength(4)]),
		email: new FormControl('', [Validators.required, Validators.email]),
		dateOfBirth: new FormControl('', [Validators.required]),
		isActive: new FormControl(''),
		goalFibers: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalCalories: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalFats: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalProteins: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalCarbohydrates: new FormControl('', [Validators.required, Validators.minLength(1)]),
		mealsADay: new FormControl('', [Validators.required, Validators.minLength(1)])
	});

	userId: string | null = null;
	currentUser: user | undefined;

	constructor(private route: ActivatedRoute, private userEditService: UserEditService, private router: Router) {

	}

	ngOnInit(): void {
		this.userId = this.route.snapshot.paramMap.get('id');
		this.userEditService.getUserById(this.userId!).subscribe((result: user) => {
			this.editUserForm.patchValue(result)
			this.editUserForm.patchValue({'goalFibers' : result.preference.goalFibers})
			this.editUserForm.patchValue({'goalFats' : result.preference.goalFats})
			this.editUserForm.patchValue({'goalCalories' : result.preference.goalCalories})
			this.editUserForm.patchValue({'goalCarbohydrates' : result.preference.goalCarbohydrates})
			this.editUserForm.patchValue({'goalProteins' : result.preference.goalProteins})
		});
	}

	onSubmit(): void {
		this.userEditService.updateUser(this.editUserForm.value, this.userId!).subscribe((result: user) => {
			this.currentUser = result
			this.router.navigate(['/user'])
		});
	}
}
