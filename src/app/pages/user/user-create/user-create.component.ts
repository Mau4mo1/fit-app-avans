import { ngModuleJitUrl } from '@angular/compiler';
import { Component, NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PopupService } from '@ng-bootstrap/ng-bootstrap/util/popup';
import { preference } from 'src/domain/preference';

import { user } from 'src/domain/user';
import { AuthenticationService } from '../../home/authentication.service';
import { UserEditService } from '../user-edit.service';

@Component({
	selector: 'app-user-create',
	templateUrl: './user-create.component.html',
	styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

	addUserForm = new FormGroup({
		name: new FormControl('', [Validators.required, Validators.minLength(4)]),
		email: new FormControl('', [Validators.required, Validators.email]),
		dateOfBirth: new FormControl('', [Validators.required]),
		isActive: new FormControl(true),
		password: new FormControl('', [Validators.required, Validators.minLength(7)]),
		goalFibers: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalCalories: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalFats: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalProteins: new FormControl('', [Validators.required, Validators.minLength(1)]),
		goalCarbohydrates: new FormControl('', [Validators.required, Validators.minLength(1)]),
		mealsADay: new FormControl('', [Validators.required, Validators.minLength(1)])
	});

	userId: string | null = null;

	constructor(private route: ActivatedRoute, private authenticationService: AuthenticationService, private router: Router) {

	}

	ngOnInit(): void {
		this.userId = this.route.snapshot.paramMap.get('id');
	}
	async onSubmit(): Promise<void> {
		//name: string, email: string, _id: string, dateOfBirth: Date, isActive: Boolean, role: number, cart: product[], preference: preference
		let newUser: user = new user(
			this.addUserForm.value.name,
			this.addUserForm.value.email,
			this.addUserForm.value.password,
			'',
			this.addUserForm.value.dateOfBirth,
			this.addUserForm.value.isActive,
			this.addUserForm.value.role,
			[],
			new preference(
				this.addUserForm.value.goalCalories,
				this.addUserForm.value.goalProteins,
				this.addUserForm.value.goalCarbohydrates,
				this.addUserForm.value.goalFats,
				this.addUserForm.value.goalFibers,
				this.addUserForm.value.mealsADay
			)
		)
		await this.authenticationService.register(newUser);
		// dit is nodig omdat anders de create na de get request wordt aangeroepen door async problemen, krijg het anders niet opgelost 😥😥
	}
}
