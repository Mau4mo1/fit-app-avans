import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { user } from 'src/domain/user';
import { UserEditService } from '../user-edit.service';


@Component({
	selector: 'app-user-details',
	templateUrl: './user-details.component.html',
	styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

	userId: string|null = null;

	currentUser:user | undefined;

	constructor(private route: ActivatedRoute, private userEditService:UserEditService) {

	}

	ngOnInit(): void {
		this.userId = this.route.snapshot.paramMap.get('id');
		this.userEditService.getUserById(this.userId!).subscribe((result: user) => this.currentUser = result);
	}

}
