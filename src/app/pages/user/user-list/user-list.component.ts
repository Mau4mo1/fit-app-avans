import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { user } from '../../../../domain/user';
import { UserEditService } from '../user-edit.service';
@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

	public users: user[] = [];
	loggedInUser: user | undefined;
	constructor(private userService: UserEditService) {

	}
	ngOnInit(): void {
		this.userService.getUsers().subscribe((results: user[]) => this.users = results)
		this.users = this.users;
	}
	delete(id: string): void {
		this.userService.deleteUser(id).subscribe({
			next: () => {
				this.users = this.users.filter(user => user._id !== id)
			}
		});
	}
	addFriend(id: string): void {
		this.userService.addFriend(id, this.userService.currentUser!._id).subscribe({

		});
	}
}


