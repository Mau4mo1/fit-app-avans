import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { user } from 'src/domain/user';
import { UserEditService } from '../user-edit.service';
import { of } from 'rxjs';
import { UserListComponent } from './user-list.component';
import { RouteConfigLoadEnd } from '@angular/router';
import { preference } from 'src/domain/preference';

let component: UserListComponent;
let fixture: ComponentFixture<UserListComponent>;
let service: jasmine.SpyObj<UserEditService>;
let httpSpy: jasmine.SpyObj<HttpClient>;

const expectedUserData: user[] = [
	{
		_id: 'mongo_id',
		name: 'maurice',
		email: 'Maurice@outlook.com',
		dateOfBirth: new Date(2001, 8, 24),
		isActive: true,
		role: 1,
		password: 'Admin123',
		preference: new preference(
			100,
			50,
			30,
			50,
			40,
			3
		),
		cart: []
	},
	{
		_id: 'mongo_id',
		name: 'Luuk',
		email: 'luuk@outlook.com',
		dateOfBirth: new Date(2001, 8, 24),
		isActive: true,
		role: 1,
		password: 'Admin123',
		preference: new preference(
			100,
			50,
			30,
			50,
			40,
			3
		),
		cart: []
	}
];

describe('UserListComponent', () => {

	beforeEach(async () => {

	});

	beforeEach(() => {
		service = jasmine.createSpyObj('UserEditService', ['getUsers', 'deleteUser'])
		httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);
		TestBed.configureTestingModule({
			declarations: [UserListComponent],
			providers: [
				{ provide: UserEditService, use: service },
				{ provide: HttpClient, useValue: httpSpy }]
		})
			.compileComponents();

		fixture = TestBed.createComponent(UserListComponent);
		component = fixture.componentInstance;

	});

	fit('should create', () => {
		httpSpy.get.and.returnValue(of(expectedUserData));

		fixture.detectChanges();

		expect(component).toBeTruthy();
		expect(component.users).toEqual(expectedUserData);
		expect(component.users.length).toEqual(expectedUserData.length);
	});
});
