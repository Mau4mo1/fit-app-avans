import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { LayoutComponent } from './core/layout/layout.component'
import { FooterComponent } from './core/footer/footer.component'
import { AboutComponent } from './pages/about/about.component'
import { UsecaseComponent } from './pages/about/usecases/usecase.component';
import { UserListComponent } from './pages/user/user-list/user-list.component';
import { UserDetailsComponent } from './pages/user/user-details/user-details.component';
import { UserEditComponent } from './pages/user/user-edit/user-edit.component'
import { ReactiveFormsModule } from '@angular/forms';
import { UserCreateComponent } from './pages/user/user-create/user-create.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SupermarketListComponent } from './pages/supermarket/supermarket-list/supermarket-list.component';
import { SupermarketEditComponent } from './pages/supermarket/supermarket-edit/supermarket-edit.component';
import { SupermarketDetailsComponent } from './pages/supermarket/supermarket-details/supermarket-details.component';
import { SupermarketCreateComponent } from './pages/supermarket/supermarket-create/supermarket-create.component';
import { CategoryDetailsComponent } from './pages/category/category-details/category-details.component';
import { CategoryCreateComponent } from './pages/category/category-create/category-create.component';
import { CategoryEditComponent } from './pages/category/category-edit/category-edit.component';
import { ProductCreateComponent } from './pages/product/product-create/product-create.component';
import { ProductDetailsComponent } from './pages/product/product-details/product-details.component';
import { ProductEditComponent } from './pages/product/product-edit/product-edit.component';
import { HomeLoginComponent } from './pages/home/home-login/home-login.component';
import { AuthInterceptor } from './pages/home/auth-interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { CartComponent } from './pages/cart/cart.component'
//import {HttpClientModule} from '@angular/common/http';

@NgModule({
	declarations: [
		AppComponent,
		NavbarComponent,
		LayoutComponent,
		DashboardComponent,
		FooterComponent,
		AboutComponent,
		UsecaseComponent,
		UserListComponent,
		UserDetailsComponent,
		UserEditComponent,
		UserCreateComponent,
		SupermarketListComponent,
		SupermarketEditComponent,
		SupermarketDetailsComponent,
		SupermarketCreateComponent,
		CategoryDetailsComponent,
		CategoryCreateComponent,
		CategoryEditComponent,
		ProductDetailsComponent,
		ProductCreateComponent,
		ProductEditComponent,
		HomeLoginComponent,
		CartComponent,
	],
	imports: [BrowserModule, RouterModule, NgbModule, AppRoutingModule, ReactiveFormsModule, HttpClientModule, BrowserAnimationsModule, MatSnackBarModule],
	providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } }],
	bootstrap: [AppComponent]
})
export class AppModule { }

