import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LayoutComponent } from './core/layout/layout.component'
import { AboutComponent } from './pages/about/about.component'
import { CategoryCreateComponent } from './pages/category/category-create/category-create.component'
import { CategoryDetailsComponent } from './pages/category/category-details/category-details.component'
import { CategoryEditComponent } from './pages/category/category-edit/category-edit.component'
import { ProductCreateComponent } from './pages/product/product-create/product-create.component'
import { ProductDetailsComponent } from './pages/product/product-details/product-details.component'
import { SupermarketCreateComponent } from './pages/supermarket/supermarket-create/supermarket-create.component'
import { SupermarketDetailsComponent } from './pages/supermarket/supermarket-details/supermarket-details.component'
import { SupermarketEditComponent } from './pages/supermarket/supermarket-edit/supermarket-edit.component'
import { SupermarketListComponent } from './pages/supermarket/supermarket-list/supermarket-list.component'
import { UserCreateComponent } from './pages/user/user-create/user-create.component'
import { UserDetailsComponent } from './pages/user/user-details/user-details.component'
import { UserEditComponent } from './pages/user/user-edit/user-edit.component'
import { UserListComponent } from './pages/user/user-list/user-list.component'
import { ProductEditComponent } from './pages/product/product-edit/product-edit.component'
import { HomeLoginComponent } from './pages/home/home-login/home-login.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
	  { path: 'login', component: HomeLoginComponent },
	  { path: 'register', component: UserCreateComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'about', component: AboutComponent },

      { path: 'user', component: UserListComponent },
      { path: 'user/create', component: UserCreateComponent },
      { path: 'user/:id', component: UserDetailsComponent },
      { path: 'user/:id/edit', component: UserEditComponent },

      { path: 'supermarket', component: SupermarketListComponent },
      { path: 'supermarket/create', component: SupermarketCreateComponent },
      { path: 'supermarket/:id', component: SupermarketDetailsComponent },
      { path: 'supermarket/:id/edit', component: SupermarketEditComponent },

	  { path: 'supermarket/:id/categories/create', component: CategoryCreateComponent },
      { path: 'supermarket/:id/categories/:categoryId', component: CategoryDetailsComponent },
	  { path: 'supermarket/:id/categories/:categoryId/edit', component: CategoryEditComponent },

	  { path: 'supermarket/:id/categories/:categoryId/:productId', component: ProductDetailsComponent },
	  { path: 'supermarket/:id/categories/:categoryId/products/create', component: ProductCreateComponent },
	  { path: 'supermarket/:id/categories/:categoryId/:productId/edit', component: ProductEditComponent },
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
