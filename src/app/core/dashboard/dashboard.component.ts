import { Component, OnInit } from '@angular/core'
import { AuthenticationService } from 'src/app/pages/home/authentication.service'
import { UserEditService } from 'src/app/pages/user/user-edit.service'
import { user } from 'src/domain/user'
import { environment } from '../../../environments/environment'

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
	runningMode: string = ''
	apiUrl: string = ''
	version: string = ''
	public currentUser: user | undefined;
	public friends: user[] | undefined;
	constructor(public authservice: AuthenticationService, public userService: UserEditService) {

	}
	async ngOnInit() {
		this.runningMode = environment.production ? 'production' : 'development'
		this.apiUrl = environment.apiUrl
		this.version = environment.version

		await this.userService.getUserById(localStorage.getItem('userId')!).subscribe(async (result: user) => {
			this.currentUser = result

			await this.userService.getFriends().subscribe((result: user[]) => {
				this.friends = result;
			})
		});
	}
}
