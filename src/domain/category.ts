import { isVariableDeclaration } from "typescript";
import { product } from "./product";

export class category {

	public name: string;
	public image: string;
	public _id: string;
	public products: product[];
	constructor(name: string, image: string, _id: string, products: product[]) {
		this.name = name;
		this.image = image;
		this._id = _id;
		this.products = products;
	}
}
