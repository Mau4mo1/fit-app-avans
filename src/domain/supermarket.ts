import { isVariableDeclaration } from "typescript";
import { category } from "./category";

export class supermarket {
	public _id: string;
	public name: string;
	public categories: category[];
	public image: string;
	
	constructor(name:string,categories:category[],_id: string,image:string) {
		this.name = name;
		this.categories = categories;
		this._id = _id;
		this.image = image;
	}
}
