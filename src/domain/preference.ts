export class preference {

	public goalCalories:Number;
	public goalProteins:Number;
	public goalCarbohydrates:Number;
	public goalFats:Number;
	public goalFibers:Number;
	public mealsADay:Number;

	constructor(goalCalories:Number,goalProteins:Number,goalCarbohydrates:Number,goalFats:Number,goalFibers:Number,mealsADay:Number){
		this.goalCalories = goalCalories;
		this.goalProteins = goalProteins;
		this.goalCarbohydrates = goalCarbohydrates;
		this.goalFats = goalFats;
		this.goalFibers = goalFibers;
		this.mealsADay = mealsADay;
	}


}
