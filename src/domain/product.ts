import { isVariableDeclaration } from "typescript";

export class product {

	public name: string;
	public image: string;
	public _id:string;
	public calories: Number;
	public protein: Number;
	public carbohydrates: Number;
	public fats: Number;
	public fibers: Number;

	constructor(name:string, image:string,_id:string,calories: Number,protein: Number,carbohydrates: Number,fats: Number,fibers: Number) {
		this.name = name;
		this.image = image;
		this._id = _id;
		this.calories = calories;
		this.protein = protein;
		this.carbohydrates = carbohydrates;
		this.fats = fats;
		this.fibers = fibers;
	}

}
