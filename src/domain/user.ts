import { isVariableDeclaration } from "typescript";
import { preference } from "./preference";
import { product } from "./product";

export class user {

	public name: string;
	public email: string;
	public password:string;
	public _id: string;
	public dateOfBirth: Date;
	public isActive: Boolean = false;
	public role: number;
	public cart: product[]
	public preference: preference;
	constructor(name: string, email: string, password:string ,_id: string, dateOfBirth: Date, isActive: Boolean, role: number, cart: product[], preference: preference) {
		this.name = name;
		this._id = _id;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
		this.isActive = isActive;
		this.email = email;
		this.role = role;
		this.cart = cart;
		this.preference = preference;
	}
}


